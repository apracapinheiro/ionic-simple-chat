import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { RoomPage } from '../room/room';
/**
 * Generated class for the SiginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sigin',
  templateUrl: 'sigin.html',
})

export class SiginPage {
  data: any = { nickname: ""};

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SiginPage');
  }

  // Função para fazer o login ou entrar em uma room
  enterNickName() {
    this.navCtrl.setRoot(RoomPage, {
      nickname: this.data.nickname
    });
}

}
