import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import * as firebase from 'firebase';

@IonicPage()
@Component({
  selector: 'page-add-room',
  templateUrl: 'add-room.html',
})
export class AddRoomPage {
  // variaveis para manter a referencia do data object e Firebase database
  data = { roomname: ''};
  ref = firebase.database().ref('chatrooms/');

  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }

  addRoom() {
      let newData = this.ref.push();
      newData.set({ roomname: this.data.roomname });
      this.navCtrl.pop();
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddRoomPage');
  }

}
