import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import * as firebase from 'firebase';

import { HomePage } from '../pages/home/home';
import { SiginPage } from '../pages/sigin/sigin';

const config = {
  apiKey: "AIzaSyBOX78B-X7ArneII-JqltiIoL2TbWT1_-k",
  authDomain: "simple-chat-b7dde.firebaseapp.com",
  databaseURL: "https://simple-chat-b7dde.firebaseio.com",
  projectId: "simple-chat-b7dde",
  storageBucket: "simple-chat-b7dde.appspot.com",
  messagingSenderId: "615958957619"
}

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = SiginPage;
  // rootPage:any = HomePage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
    firebase.initializeApp(config);
  }
}

